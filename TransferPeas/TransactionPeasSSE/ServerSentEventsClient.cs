﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransactionPeasSSE
{
    public class ServerSentEventsClient
    {
        private readonly HttpResponse _response;

        internal ServerSentEventsClient(HttpResponse response)
        {
            _response = response;
        }

        public Task SendEventAsync(ServerSentEvent serverSentEvent)
        {
            return _response.WriteSseEventAsync(serverSentEvent);
        }
    }
}
