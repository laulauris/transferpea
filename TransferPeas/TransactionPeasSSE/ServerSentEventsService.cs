﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransactionPeasSSE
{
    public class ServerSentEventsService
    {
        private readonly ConcurrentDictionary<Guid, ServerSentEventsClient> _clients = new ConcurrentDictionary<Guid, ServerSentEventsClient>();

        internal Guid AddClient(ServerSentEventsClient client)
        {
            Guid clientId = Guid.NewGuid();

            _clients.TryAdd(clientId, client);

            return clientId;
        }

        internal void RemoveClient(Guid clientId)
        {
            ServerSentEventsClient client;

            _clients.TryRemove(clientId, out client);
        }

        public Task SendEventAsync(ServerSentEvent serverSentEvent)
        {
            List<Task> clientsTasks = new List<Task>();
            foreach (ServerSentEventsClient client in _clients.Values)
            {
                clientsTasks.Add(client.SendEventAsync(serverSentEvent));
            }

            return Task.WhenAll(clientsTasks);
        }
    }
}
