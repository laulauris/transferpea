﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transactions
{
    public interface IPeaTransaction
    {
        List<double> StartLocation { get; set; }
        List<double> EndLocation { get; set; }
        double Amount { get; set; }
        double Saved { get; set; }
    }
}
