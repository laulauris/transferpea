﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transactions
{
    public class TransactionDto
    {
        public long Counter { get; set; }
        public List<double> StartPlace { get; set; }
        public List<double> EndPlace { get; set; }
        public double Amount { get; set; }
        public double Saved { get; set; }
        public string TimeStamp { get; set; }
    }
}
