﻿using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Text;

namespace TransferPeas.Beams
{
    public interface IBeam
    {
        List<Coordinate> StartLocations { get; set; }
        List<Coordinate> EndLocations { get; set; }
        double BeamWeight { get; set; }
    }
}
