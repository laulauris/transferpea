﻿using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Text;

namespace TransferPeas.Beams
{
    public class Coordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    public class Beam : IBeam
    {
        public List<Coordinate> StartLocations { get; set; }
        public List<Coordinate> EndLocations { get; set ; }
        public double BeamWeight { get; set; }
    }
}
