﻿using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using GeoCoordinatePortable;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Transactions;
using TransferPeas.Beams;
using TransferPeas.Transactions;

namespace TransferPeas
{
    class Program
    {
        public static double savedToday = 18999.85;
        public static double savedLastHour = 0;
        public static long offset = 0;

        static void Main(string[] args)
        {
            var config = new Dictionary<string, object>
            {
                { "group.id", "sample-consumer" },
                { "bootstrap.servers", "192.168.81.25:9092" },
                { "enable.auto.commit", "false"}
            };

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            HttpClient azureClient = new HttpClient();
            azureClient.BaseAddress = new Uri("https://transferpeaapp.azurewebsites.net");
            azureClient.DefaultRequestHeaders.Accept.Clear();
            azureClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            List<TransactionDto> messages = new List<TransactionDto>();
            int offsetMilliseconds = 0;

            using (var consumer = new Consumer<Null, string>(config, null, new StringDeserializer(Encoding.UTF8)))
            {
                consumer.Subscribe(new string[] { "Topic1" });

                consumer.OnMessage += (_, msg) =>
                {
                    Console.WriteLine($"Topic: {msg.Topic} Partition: {msg.Partition} Offset: {msg.Offset} {msg.Value}");

                    try
                    {
                        TransactionDto transaction = JsonConvert.DeserializeObject<TransactionDto>(msg.Value);
                        transaction.Counter = msg.Offset.Value;
                        messages.Add(transaction);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    var commited = consumer.CommitAsync(msg);
                };

                while (true)
                {
                    if(offsetMilliseconds >= 2000 && messages.Count > 0)
                    {
                        offsetMilliseconds = 0;

                        string transactionJson = GetTransactionJson(messages.Where(m => m.Counter > offset).ToList());
                        if (!string.IsNullOrEmpty(transactionJson))
                        {
                            offset = messages.Last().Counter;
                            Task.Run(async () => await CreateMessageAsync(client, transactionJson));
                            Task.Run(async () => await CreateMessageAsync(azureClient, transactionJson));
                            Thread.Sleep(1000);
                            messages.Clear();
                        }
                    }
                    consumer.Poll(100);
                    offsetMilliseconds += 10;
                }
            }
        }

        private static string GetTransactionJson(List<TransactionDto> messages)
        {
            IBeam beam = new Beam
            {
                StartLocations = new List<Coordinate>(),
                EndLocations = new List<Coordinate>(),
                BeamWeight = 0
            };

            TransactionDto first = messages.First();
            beam.StartLocations.Add(GetStartGeoCoordinates(first));
            beam.EndLocations.Add(GetEndGeoCoordinates(first));

            DateTime now = DateTime.Now;
            foreach (var message in messages)
            {
                if (message == first)
                    continue;

                double startDistance = DistanceCalculator.DistanceInKmBetweenEarthCoordinates(message.StartPlace[0], message.StartPlace[1], first.StartPlace[0], first.StartPlace[1]);
                if(startDistance <= 500)
                {
                    beam.StartLocations.Add(GetStartGeoCoordinates(message));
                    beam.BeamWeight += message.Amount;
                }

                double endDistance = DistanceCalculator.DistanceInKmBetweenEarthCoordinates(message.EndPlace[0], message.EndPlace[1], first.EndPlace[0], first.EndPlace[1]);
                if (endDistance <= 500)
                {
                    beam.EndLocations.Add(GetEndGeoCoordinates(message));
                    beam.BeamWeight += message.Amount;
                }

                string date = message.TimeStamp;
                DateTime dateTime = DateTime.Parse(date);

                if(now.AddHours(-1) < dateTime)
                {
                    savedLastHour += message.Saved;
                }
                if (dateTime.Day == now.Day)
                {
                    savedToday += message.Saved;
                }
            }

            if(beam.EndLocations.Count <=1 && beam.StartLocations.Count <= 1)
            {
                return null;
            }

            ITransaction transaction = new Transaction();
            transaction.Beam = beam;
            transaction.SavedLastHour = savedLastHour;
            transaction.SavedToday = savedToday;

            return transaction != null ? JsonConvert.SerializeObject(transaction) : null;
        }

        private static Coordinate GetStartGeoCoordinates(TransactionDto message)
        {
            return new Coordinate
            {
                Latitude = message.StartPlace[0],
                Longitude = message.StartPlace[1]
            };
        }

        private static Coordinate GetEndGeoCoordinates(TransactionDto message)
        {
            return new Coordinate
            {
                Latitude = message.EndPlace[0],
                Longitude = message.EndPlace[1]
            };
        }

        static async Task<Uri> CreateMessageAsync(HttpClient client, string message)
        {
            HttpResponseMessage response = await client.PostAsync(
                "notifications/sse-notifications-sender", new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Notification", message),
                    new KeyValuePair<string, string>("Alert", "false")
                }));
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }
    }
}
