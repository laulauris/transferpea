﻿using System;
using System.Collections.Generic;
using System.Text;
using TransferPeas.Beams;

namespace TransferPeas.Transactions
{
    public class Transaction : ITransaction
    {
        public IBeam Beam { get; set; }
        public double SavedLastHour { get; set; }
        public double SavedToday { get; set; }
    }
}
