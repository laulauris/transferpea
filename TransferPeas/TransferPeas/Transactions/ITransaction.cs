﻿using System;
using System.Collections.Generic;
using System.Text;
using TransferPeas.Beams;

namespace TransferPeas.Transactions
{
    public interface ITransaction
    {
        IBeam Beam { get; set; }
        double SavedLastHour { get; set; }
        double SavedToday { get; set; }
    }
}
