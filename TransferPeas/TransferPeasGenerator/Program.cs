﻿using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using System;
using System.Collections.Generic;
using System.Text;
using Transactions;

namespace TransferPeasGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new Dictionary<string, object>
            {
                { "bootstrap.servers", "192.168.81.25:9092" }
            };

            using (var producer = new Producer<Null, string>(config, null, new StringSerializer(Encoding.UTF8)))
            {
                string text = null;

                while (text != "exit")
                {
                    TransactionDto transaction = new TransactionDto();
                    Console.WriteLine("StartPlace EndPlace Amount Saved TimeStamp");
                    text = Console.ReadLine();
                    producer.ProduceAsync("Topic1", null, text);
                }

                var result = producer.Flush(100);
            }
        }
    }
}
