# Demo.AspNetCore.ServerSentEvents:

.NET Core Web API to handle server sent events (SSE)
endpoints:

[Get] notifications/sse-notifications-receiver

This endpoint is listening to Server to receive Transaction messages, that are ready to be visualized

# TransferPeas:

.NET Core console application that is handling the message processing

1) It is listening to Kafka server "Topic1" to receive generated Transactions

2) It is processing the Transactions, calculating the saved money today and in last hour

3) It is sending calculated messages to Server using SSE. These messages can be read from notifications/sse-notifications-receiver endpoint
